from django.db import IntegrityError
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

# Create your views here.


def logOK(request):
    username = request.user.username
    return render(request, 'logOK.html', {'username': username})


def algo(request):
    return render(request, 'algorar.html')


# def home(request):
    print("hola")
    return render(request, 'home.html')


def closeSesion(request):
    logout(request)
    print("PRECIO CERRAR SECION")
    return redirect('home')


def singup(request):
    if request.method == 'GET':
        print("soy el get")
        return render(request, 'singup.html', {
            'form': UserCreationForm})
    else:
        if request.POST['password1'] == request.POST['password2']:
            try:
                print("ENTRE AL TRY")
                user = User.objects.create_user(username=request.POST['username'],
                                                password=request.POST['password1'])
                print("hola")
                user.save()
                print("hola")
                login(request, user)
                return redirect('logOK')
            except IntegrityError:
                return render(request, 'singup.html', {
                    'form': UserCreationForm,
                    "error": "usuario ya existe"
                })
        return render(request, 'singup.html', {
            'form': UserCreationForm,
            "error": "las contrasenas no coinciden"
        })


def signin(request):
    if request.method == 'GET':
        print("soy el get de loguearse")
        return render(request, 'loguearse.html', {
            'form': AuthenticationForm
        })
    else:
        print("intentando loguearse...")

        user = authenticate(
            request, username=request.POST['username'], password=request.POST['password'])
        if user is None:
            print("el usuario no existe")
            return render(request, 'loguearse.html', {
                'form': AuthenticationForm,
                "error": "el usuario no existe, o la contraseña no es correcta."
            })
        else:
            print("el usuario si existe y puede loguearse")
            user.save()
            print("usuario guardado en la base de datos....")
            login(request, user)
            username = request.user.username
            # return redirect('logOK')
            return render(request, 'logOK.html', {'username': username})
       # return render(request, 'logueado.html', {
       #     'form': AuthenticationForm
