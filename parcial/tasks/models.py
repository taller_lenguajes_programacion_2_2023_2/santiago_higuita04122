from django.db import models


class Persona(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    fecha_nacimiento = models.DateField()
    direccion = models.CharField(max_length=200)
    email = models.EmailField()
    telefono = models.CharField(max_length=20)

    def __str__(self):
        return f'{self.nombre} {self.apellido}'

    def set_nombre(self, nuevo_nombre):
        self.nombre = nuevo_nombre
        self.save()

    def get_nombre(self):
        return self.nombre


class Usuario(Persona):
    nombre_de_usuario = models.CharField(max_length=30, unique=True)
    contrasena = models.CharField(max_length=100)
    fecha_registro = models.DateTimeField(auto_now_add=True)

    def get_nombre_de_usuario(self):
        return self.nombre_de_usuario

    def set_nombre_de_usuario(self, nuevo_nombre_de_usuario):
        self.nombre_de_usuario = nuevo_nombre_de_usuario
        self.save()

    def get_contrasena(self):
        return self.contrasena

    def set_contrasena(self, nueva_contrasena):
        self.contrasena = nueva_contrasena
        self.save()


# Create your models here.
