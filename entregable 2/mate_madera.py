from mate import Mate  # Importar la clase base Mate del archivo mate.py


class MateMadera(Mate):
    def __init__(self, id_mate, descripcion, costo, tipo_madera):
        super().__init__(id_mate, descripcion, costo)
        self.tipo_madera = tipo_madera

    _modelo = None
    _tipo_madera = None
    _elaboracion = None   

    def get_modelo(self):
        return self._modelo
    
    def set_modelo(self, value):
        self._modelo = value
   

  

    def set_tipo_madera(self, value):
        self.tipo_madera = value    

    def get_tipo_madera(self):
        return self._tipo_madera
        

    
    def set_elaboracion(self, value):
        self._elaboracion = value
    
    def get_elaboracion(self):
        return self._elaboracion
    
   
            

mate2=MateMadera(3,"Mate madera",500,"roble")

