class Mate:
    def __init__(self, id_mate, descripcion, costo, largo=None, ancho=None, diametro=None, peso=None, capacidad=None, cuidados=None, acabados=None, origen=None, proveedor=None, garantia=None):
        self.__id_mate = id_mate
        self.__descripcion = descripcion
        self.__costo = costo
        self.__peso = peso
        self.__largo = largo
        self.__ancho = ancho
        self.__diametro = diametro
        self.__capacidad = capacidad
        self.__cuidados = cuidados
        self.__acabados = acabados
        self.__origen = origen
        self.__proveedor = proveedor
        self.__garantia = garantia

 # Getters
    def get_id_mate(self):
        return self.__id_mate

    def get_descripcion(self):
        return self.__descripcion

    def get_costo(self):
        return self.__costo

    def get_largo(self):
        return self.__largo

    def get_ancho(self):
        return self.__ancho

    def get_diametro(self):
        return self.__diametro

    def get_peso(self):
        return self.__peso

    def get_capacidad(self):
        return self.__capacidad

    def get_cuidados(self):
        return self.__cuidados

    def get_acabados(self):
        return self.__acabados

    def get_origen(self):
        return self.__origen

    def get_proveedor(self):
        return self.__proveedor

    def get_garantia(self):
        return self.__garantia

    # Setters
    def set_id_mate(self, id_mate):
        self.__id_mate = id_mate

    def set_descripcion(self, descripcion):
        self.__descripcion = descripcion

    def set_costo(self, costo):
        self.__costo = costo

    def set_largo(self, largo):
        self.__largo = largo

    def set_ancho(self, ancho):
        self.__ancho = ancho

    def set_diametro(self, diametro):
        self.__diametro = diametro

    def set_peso(self, peso):
        self.__peso = peso

    def set_capacidad(self, capacidad):
        self.__capacidad = capacidad

    def set_cuidados(self, cuidados):
        self.__cuidados = cuidados

    def set_acabados(self, acabados):
        self.__acabados = acabados

    def set_origen(self, origen):
        self.__origen = origen

    def set_proveedor(self, proveedor):
        self.__proveedor = proveedor

    def set_garantia(self, garantia):
        self.__garantia = garantia  

#mate1 = Mate(1, "Mate de cerámica", 15.0)

#mate1.set_ancho(5)
#mate1.__ancho=3000
#print("holaaa")


#ejemplo 
#print(mate1.get_ancho())
