import sqlite3 as sql
from mate import Mate
from mate_madera import MateMadera
def createDB():
    conn=sql.connect("mates.db")
    conn.commit()
    conn.close()

def createTable():
    conn=sql.connect("mates.db")
    cursor=conn.cursor()
    cursor.execute(
        """CREATE TABLE mate_madera (
            id_mate text,
            descripcion text,
            costo decimal(10,2),
            peso decimal(10,2),
            largo decimal(10,2),
            ancho decimal(10,2),
            diametro decimal(10,2),
            capacidad decimal(10,2),
            cuidados text,
            acabados text,
            modelo text,
            tipo_madera text,
            elacoracion text,
            origen text,
            proveedor text,
            garantia integer 
        )"""
    )
    conn.commit()
    conn.close()


#MateMadera(3,"Mate madera",500,"roble")

def deleteTableMateMadera():     
    conn=sql.connect("mates.db")
    cursor=conn.cursor()
    cursor.execute(
        """DROP TABLE mate_madera"""
    )
    conn.commit()
    conn.close()

def insertMateMadera(mate):
    print(mate.get_id_mate())
    print(mate.get_descripcion())
    print(mate.get_costo())
  
    conn=sql.connect("mates.db")
    cursor=conn.cursor()
    instruccion = f"INSERT INTO mate_madera (id_mate, descripcion, costo, peso, largo, ancho, diametro, capacidad, cuidados, acabados, modelo, tipo_madera, elacoracion, origen, proveedor, garantia) VALUES ('{mate.get_id_mate()}', '{mate.get_descripcion()}', {mate.get_costo()}, {mate.get_peso()}, {mate.get_largo()}, {mate.get_ancho()}, {mate.get_diametro()}, {mate.get_capacidad()}, '{mate.get_cuidados()}', '{mate.get_acabados()}', '{mate.get_modelo()}', '{mate.get_tipo_madera()}', '{mate.get_elaboracion()}', '{mate.get_origen()}', '{mate.get_proveedor()}', {mate.get_garantia()})"

    #La consulta SQL para insertar un MateMadera
    cursor.execute(instruccion)

    conn.commit()
    #print("Mate de madera insertado exitosamente.")
    conn.close()

if __name__==   "__main__":
    #createDB()
    #deleteTableMateMadera()
    #createTable()
    nuevo_mate = MateMadera(1113, "Mate madera", 500, "Madera")


# Llamar a la función para insertar el MateMadera en la base de datos
    insertMateMadera(nuevo_mate)

# Cerrar la conexión a la base de datos



